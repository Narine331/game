import React from 'react';
import range from 'lodash/range';
import shuffle from 'lodash/shuffle';

import Tile from './components/Tile';
import Board from './components/Board';
import Group from './components/Group';
import Button from './components/Buttons';

import createGrid from './helpers/createGrid';

import { COL, ROW, GAP, BUTTON_SIZE } from './config';
import  './App.css';

class App extends React.Component {
  state = {
    tiles: range(0, COL * ROW),
    size: (BUTTON_SIZE * COL) + ((COL + 1) * GAP),
    grid: createGrid(COL, ROW, BUTTON_SIZE, GAP),
    coordinates: shuffle(range(0, COL * ROW)),
    history: [],
    step:0
  }

  handleReset = () => this.setState({ coordinates: shuffle(this.state.tiles) });

  handleSlove = () => {
    this.setState({
      coordinates: [...range(1, COL * ROW), 0],
    }, () => {
      this.handleCheckGrid();
    });
  }

  handleCheckGrid() {
    const { tiles, coordinates } = this.state;
  }

  handleSaveMove(coordinates) {
    this.setState({
      history: [
        ...this.state.history,
        coordinates,
      ]
    });
  }

  handleTakeMoveBack = () => {
    const { history } = this.state;
    const coordinates = history.pop();

    if (coordinates == null) { return false; }

    this.setState({
      history,
      coordinates,
    });
  }

  moveTile(index) {
    if (!index) return null;

    const { coordinates } = this.state;
    const emptyTile = coordinates.indexOf(0);
    const movedTile = coordinates.indexOf(index);
    const dif = Math.abs(movedTile - emptyTile);

    if (dif === 1 || dif === ROW) {
      const previosCoordinates = coordinates.slice(0);

      coordinates[emptyTile] = index;
      coordinates[movedTile] = 0;
      const step=this.state.step + 1;
      this.setState({
        step
      });
 
      this.setState({ coordinates }, () => {
        this.handleCheckGrid();
        this.handleSaveMove(previosCoordinates);
      });
    }
  };

  render() {
    const { size, grid, tiles, coordinates,step } = this.state;

    return (
      step !== 300?  <section>
        <Board size={size}>
          {tiles.map(
            index =>
            <Tile
              coordinates={grid[coordinates.indexOf(index)]}
              key={index}
              index={index}
              size={BUTTON_SIZE}
              gap={GAP}
              onClick={this.moveTile.bind(this, index)}
            >{index}</Tile>
          )}
          {tiles.map(
            index =>
            <Tile
              coordinates={grid[index]}
              key={index}
              index={0}
              size={BUTTON_SIZE}
              gap={GAP}
     
            />
          )}
        </Board>
        <Group>
          <Button onClick={this.handleSlove}>Slove</Button>
          <Button onClick={this.handleReset}>Reset</Button>
          <Button onClick={this.handleTakeMoveBack}>previos</Button>
          <Button >Steps:{step}<br />(you have 300 steps)</Button>
        </Group>
     
      </section> : <section>
    
        <Group>
          <Button onClick={this.handleSlove}>Slove</Button>
          <Button onClick={this.handleReset}>Reset</Button>
          <Button onClick={this.handleTakeMoveBack}>previos</Button>
          <Button >Steps:{step}</Button>
        </Group>
        <div className="gameOver"> Game Over </div>
      </section>
      
    );
  }
}

export default App;