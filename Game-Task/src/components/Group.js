/**
 * Vendor
 */

import styled from 'styled-components';

/**
 * Expo
 */

const Groups = styled.section`
  display: flex;
  justify-content: space-between;
  padding: 10px;
  background-color: green;
  border-radius: 10px;
`;

export default Groups;