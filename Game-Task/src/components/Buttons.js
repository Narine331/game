import styled from 'styled-components';

const Button = styled.button`
  padding: 5px 10px;
  text-transform: uppercase;
  background-color: orange;
  border-radius: 4px;
  border: 1px solid red;
  color:#000;
  cursor:pointer;
`;

export default Button;