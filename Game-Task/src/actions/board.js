/*!
 * Vendor
 */

import { createAction } from 'redux-act';

/*!
 * Expo
 */

/**
 * Reset
 */


export const requestReset = createAction('REQUEST_RESET');
export const receiveReset = createAction('RECEIVE_RESET');


export const reset = () => (dispatch) => {
  dispatch(requestReset());
  
  dispatch(receiveReset());
};

/**
 * Slove
 */

export const requestSlove = createAction('REQUEST_SLOVE_BUTTON');
export const receiveSlove = createAction('RECEIVE_SLOVE_BUTTON');

export const slove = data => (dispatch) => {
  dispatch(requestSlove());
};

/**
 * MOVE TILE
 */

export const requestMoveButton = createAction('REQUEST_MOVE_BUTTON');
export const receiveMoveButton = createAction('RECEIVE_MOVE_BUTTON');

export const moveTile = data => (dispatch) => {
  dispatch(requestMoveButton());
};